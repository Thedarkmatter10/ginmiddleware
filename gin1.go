package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
)
 
func main(){
	router := gin.Default()

	basicAuth := gin.BasicAuth(gin.Accounts{

		"lorem" : "ipsum",
	})
   authorized := router.Group("/", basicAuth)

   authorized.GET("/welcome", func(c *gin.Context){
     c.JSON(http.StatusOK,gin.H{
        "message" : "welcome to middleware",
	 })

   })
	router.Run(":8081")
}